const express = require("express");
const fileUpload = require("express-fileupload");
const http = require("http");
const socketIO = require("socket.io");
const { Client, MessageMedia, LocalAuth } = require("whatsapp-web.js");
const qrcode = require("qrcode");
const {
  query,
  validationResult,
  body,
  sanitizeBody,
} = require("express-validator");
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const swaggerDoc = require("swagger-jsdoc");

const port = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const swaggerOption = {
  swaggerDefinition: {
    info: {
      title: "Whatsapp web rest api",
      version: "1.0.0",
    },
  },
  servers: [`http:localhost:${port}`],
  apis: ["app.js"],
};

const swaggerDocs = swaggerJSDoc(swaggerOption);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

const phoneNumberFormatter = function (number) {
  let formatted = number.replace(/\D/g, "");

  if (formatted.startsWith("0")) {
    formatted = "62" + formatted.substr(1);
  }

  if (!formatted.endsWith("@c.us")) {
    formatted += "@c.us";
  }

  return formatted;
};

app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.static("./"));

app.use(
  fileUpload({
    debug: true,
  })
);

/* app.use((req, res, next) => {
    res.status(404).json({
        status: '404',
        message: 'Invalid Method'
    })
});  */

const client = new Client({
  restartOnAuthFail: true,
  puppeteer: {
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-dev-shm-usage",
      "--disable-accelerated-2d-canvas",
      "--no-first-run",
      "--no-zygote",
      "--single-process", // <- this one doesn't works in Windows
      "--disable-gpu",
    ],
  },
  authStrategy: new LocalAuth(),
});

/* client.on('message', msg => {
    if(msg.body == 'group-info'){
        msg.reply('Cant get info for now ... ')
    }
    console.log(`new message ${msg.body}`);
}); */

client.initialize();

// socket io on connection
io.on("connection", async function (socket) {
  socket.on("logout", async function (param) {
    socket.emit("log", "Logging out ...");
    client.logout();
    // client.destroy();
    client.initialize();
  });

  let sessionCheck = true;

  socket.emit("init", "Initializing ... ");
  socket.emit("log", "Initializing ... ");

  client.on("qr", async (qr) => {
    // qrTerminal.generate(qr, {small:true})
    qrcode.toDataURL(qr, (err, url) => {
      socket.emit("qr", url);
      socket.emit("log", "Scan QR code to authentication ...");
    });
    sessionCheck = false;
    // console.log('QR code received')
  });

  client.on("authenticated", async (par) => {
    socket.emit("authenticated", "Authenticated");
    socket.emit("log", "Successfully Authenticated");
    console.log("Authenticated");
  });

  client.on("ready", async () => {
    socket.emit("ready", "System is ready");
    socket.emit("log", "Rest api is ready");
    socket.emit("log", "Connected");
    
    if(sessionCheck){
      socket.emit("userInfo", client.info);
    }
    
    console.log("Ready");
  });

  client.on("auth_failure", async (session) => {
    socket.emit("log", "Authentication failure, try again after a while ...");
  });

  client.on("disconnected", async (reason) => {
    socket.emit("log", "Disconnected from whatsapp ...");
    client.destroy();
    client.initialize();
  });

  if (sessionCheck) {
    socket.emit("authenticated", "Authenticated");
    socket.emit("log", "Successfully Authenticated");
    socket.emit("log", "Rest api is ready");
    socket.emit("log", "Connected");
    socket.emit("userInfo", client.info);
  } 
});

app.get("/", (req, res) => {
  res.sendFile("index.html", {
    root: __dirname,
  });
});

/**
 * @swagger
 * /group-info:
 *  get:
 *     description: get all group info
 *     responses:
 *      200:
 *       description: Success retrieve all group info
 *
 *
 */
app.get("/group-info", (req, res) => {
  let resArray = { status: "Failed", message: "error", data: "" };

  client.getChats().then((chats) => {
    const groups = chats.filter((chat) => chat.isGroup);

    if (groups.length == 0) {
      resArray.status = "Ok";
      resArray.message = "No data found";
    } else {
      let dataGroup = [];

      groups.forEach((group, i) => {
        dataGroup.push({ id: group.id._serialized, name: group.name });
      });

      resArray.status = "Ok";
      resArray.message = "Use group ID to send message to the group";
      resArray.data = dataGroup;
    }
    res.status(200).json(resArray);
  });
});

/**
 * @swagger
 * /user-info:
 *  get:
 *     description: get user info
 *     responses:
 *      200:
 *       description: Success retrieve user info
 *
 *
 */
app.get("/user-info", (req, res) => {
  return res.json({
    status: true,
    data: client.info,
  });
});

const checkRegisteredNumber = async function (number) {
  const isRegistered = await client.isRegisteredUser(number);
  return isRegistered;
};

/**
 * @swagger
 * /send-get:
 *  get:
 *     summary: Sending whatsapp message
 *     parameters:
 *      - name : to
 *        in : query
 *        type : string
 *        required : true
 *        description : Phone number receiver (start with +62 or 0 )
 *      - name : message
 *        in : query
 *        type : string
 *        required : true
 *        description : The message
 *      - name : isGroup
 *        in : query
 *        type : boolean
 *        required : true
 *        description : true / false
 *     responses:
 *      200:
 *       description: Success send message
 *      422:
 *       description: incomplete parameters / receiver number is not valid
 *      500:
 *       description: failed to send message due to system failure
 *
 */
app.get(
  "/send-get",
  [query("to").notEmpty(), query("message").notEmpty()],
  async (req, res) => {
    const errors = validationResult(req).formatWith(({ msg }) => {
      return msg;
    });

    if (!errors.isEmpty()) {
      return res.status(422).json({
        status: false,
        message: errors.mapped(),
      });
    }
    let number = req.query["to"];
    let message = req.query["message"];

    if (!req.query["isGroup"] || req.query["isGroup"] != 'true') {
      number = phoneNumberFormatter(number);

      const isRegisteredNumber = await checkRegisteredNumber(number);

      if (!isRegisteredNumber) {
        return res.status(422).json({
          status: false,
          message: "The number is not registered",
        });
      }
    }

    client
      .sendMessage(number, message)
      .then((response) => {
        res.status(200).json({
          status: true,
          response: response,
        });
      })
      .catch((err) => {
        res.status(500).json({
          status: false,
          response: err,
        });
      });
  }
);

/**
 * @swagger
 * /send-post:
 *  post:
 *     summary: Sending whatsapp message
 *     requestBody:
 *     parameters:
 *          - name: to
 *            in: formData
 *            format: string
 *            example: '081234567890'
 *          - name: message
 *            in: formData
 *            format: string
 *            example: 'test message from rest api'
 *          - name: isGroup
 *            in: formData
 *            type: boolean
 *            format: boolean
 *            example : 'true / false'
 *     responses:
 *      200:
 *       description: Success send message
 *      422:
 *       description: incomplete parameters / receiver number is not valid
 *      500:
 *       description: failed to send message due to system failure
 *
 */
app.post("/send-post", async (req, res) => {
  /* req.body.to.notEmpty();
  req.body.message.notEmpty(); */
  const errors = validationResult(req).formatWith(({ msg }) => {
    return msg;
  });

  if (!errors.isEmpty()) {
    return res.status(422).json({
      status: false,
      message: errors.mapped(),
    });
  }
  let number = req.body.to;
  let message = req.body.message;

  if (!req.body.isGroup) {
    number = phoneNumberFormatter(number);

    const isRegisteredNumber = await checkRegisteredNumber(number);

    if (!isRegisteredNumber) {
      return res.status(422).json({
        status: false,
        message: "The number is not registered",
      });
    }
  }

  client
    .sendMessage(number, message)
    .then((response) => {
      res.status(200).json({
        status: true,
        response: response,
      });
    })
    .catch((err) => {
      res.status(500).json({
        status: false,
        response: err,
      });
    });
});

/**
 * @swagger
 * /send-file:
 *  post:
 *     summary: Sending file
 *     requestBody:
 *       consume:
 *          - multipart/form-data
 *       content:
 *          multipart/form-data:
 *              schema:
 *                type: object
 *                properties:
 *                  file:
 *                      type: file
 *                      format: binary
 *     parameters:
 *          - name: to
 *            in: formData
 *            format: string
 *            example: '081234567890'
 *          - name: caption
 *            in: formData
 *            format: string
 *            example: 'caption file'
 *          - name: file
 *            in: formData
 *            type: file
 *            format: file
 *     responses:
 *      200:
 *       description: Success send message
 *      422:
 *       description: incomplete parameters / receiver number is not valid
 *      500:
 *       description: failed to send message due to system failure
 *
 */
app.post("/send-file", async (req, res) => {
  // const number = phoneNumberFormatter(req.body.number);
  const number = req.body.to;
  const caption = req.body.caption;
  const file = req.files.file;
  const media = new MessageMedia(
    file.mimetype,
    file.data.toString("base64"),
    file.name
  );

  client
    .sendMessage(number, media, {
      caption: caption,
    })
    .then((response) => {
      res.status(200).json({
        status: true,
        response: response,
      });
    })
    .catch((err) => {
      res.status(500).json({
        status: false,
        response: err,
      });
    });
});

server.listen(port, function () {
  console.log("App running on *: " + port);
});
